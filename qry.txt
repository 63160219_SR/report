SELECT * FROM invoices
LEFT JOIN invoice_items
ON invoices.InvoiceId = invoice_items.InvoiceLineId


SELECT strftime("%Y-%m-%d" ,InvoiceDate) as period, SUM(Total) as total FROM invoices
LEFT JOIN invoice_items
ON invoices.InvoiceId = invoice_items.InvoiceId
GROUP BY period
ORDER BY period DESC